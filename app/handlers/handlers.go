package handlers

import (
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	"gitlab.com/rmoe/tradebinder/app/models"
	"golang.org/x/crypto/bcrypt"
)

func ListCards(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	var cards []models.Card
	db.Find(&cards)

	rs, _ := json.Marshal(&cards)
	w.Header().Set("Content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(rs))
}

func AddCard(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	var card models.Card

	body, _ := ioutil.ReadAll(r.Body)
	err := json.Unmarshal(body, &card)
	if err != nil {
		fmt.Println(err)
	}

	db.Create(&card)
}

func DeleteCard(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	cardID, _ := strconv.ParseUint(params["id"], 10, 32)

	card := models.Card{
		ID: uint(cardID),
	}

	db.Delete(&card)
	w.WriteHeader(http.StatusNoContent)
}

func UpdateCard(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	cardID, _ := strconv.ParseUint(params["id"], 10, 32)

	body, _ := ioutil.ReadAll(r.Body)

	var card models.Card
	json.Unmarshal(body, &card)
	card.ID = uint(cardID)

	db.Save(&card)
	w.WriteHeader(http.StatusNoContent)
}

func AddUser(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		respondWithError(http.StatusInternalServerError, "Failed to parse body", w)
	}

	var user models.User
	var existingUser models.User
	json.Unmarshal(body, &user)

	db.Find(&existingUser, "username = ?", user.Username)
	if existingUser.ID != 0 {
		respondWithError(http.StatusConflict, "User already exists", w)
		return
	}

	password, err := bcrypt.GenerateFromPassword([]byte(user.Password), 16)
	user.Password = string(password)

	if err != nil {
		respondWithError(http.StatusInternalServerError, "Failed to hash password", w)
	}

	db.Create(&user)
	w.WriteHeader(http.StatusOK)
}

func AuthUser(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		respondWithError(http.StatusInternalServerError, "Failed to parse body", w)
	}

	var user models.User
	var existingUser models.User
	json.Unmarshal(body, &user)

	db.Where("username = ?", user.Username).Find(&existingUser)
	hashed := bcrypt.CompareHashAndPassword([]byte(existingUser.Password), []byte(user.Password))

	if hashed != nil {
		respondWithError(http.StatusUnauthorized, "Incorrect username or password", w)
		return
	}

	token := make([]byte, 32)
	rand.Read(token)
	output := fmt.Sprintf("{token: %s}", base64.StdEncoding.EncodeToString(token))
	jsonResponse(w, output)
}

func jsonResponse(w http.ResponseWriter, body string) {
	w.Header().Set("Content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(body))
}

func respondWithError(status int, error string, w http.ResponseWriter) {
	w.WriteHeader(status)
	w.Write([]byte(error))
}
