module gitlab.com/rmoe/tradebinder

go 1.12

require (
	github.com/DATA-DOG/go-sqlmock v1.3.3
	github.com/Netflix/go-env v0.0.0-20180529183433-1e80ef5003ef
	github.com/google/go-cmp v0.2.0
	github.com/gorilla/mux v1.7.3
	github.com/jinzhu/gorm v1.9.10
	golang.org/x/crypto v0.0.0-20190325154230-a5d413f7728c
)
