package handlers_test

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"

	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/google/go-cmp/cmp"
	"github.com/jinzhu/gorm"
	"gitlab.com/rmoe/tradebinder/app"
	"gitlab.com/rmoe/tradebinder/app/models"
	"golang.org/x/crypto/bcrypt"
)

func newTestSetup() (app.App, sqlmock.Sqlmock) {
	mockdb, mock, _ := sqlmock.New()
	db, err := gorm.Open("postgres", mockdb)
	if err != nil {
		panic(err)
	}
	a := app.App{}
	a.Initialize(db)

	return a, mock
}

func makeRequest(app app.App, verb string, path string, body io.Reader) *httptest.ResponseRecorder {
	req := httptest.NewRequest(verb, path, body)
	res := httptest.NewRecorder()
	app.Router.ServeHTTP(res, req)

	return res
}

func TestAddCardHandler(t *testing.T) {
	a, mock := newTestSetup()
	defer a.DB.Close()

	mock.ExpectBegin()
	row := sqlmock.NewRows([]string{"name", "quantity", "set"}).
		AddRow("test", 5, "ABC")
	mock.ExpectQuery("^INSERT (.+)").WillReturnRows(row)

	data := []byte(`{"name":"test","quantity":5, "set": "ABC"}`)
	body := bytes.NewReader(data)

	res := makeRequest(a, "POST", "/cards", body)

	if res.Code != http.StatusOK {
		t.Errorf("Expected Status Code %d got %d", http.StatusOK, res.Code)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("Expectations not met: %s", err)
	}
}

func TestListCardHandler(t *testing.T) {
	a, mock := newTestSetup()
	defer a.DB.Close()

	// Insert data into the mock db
	rows := sqlmock.NewRows([]string{"name", "quantity", "set"}).
		AddRow("test", 5, "ABC").
		AddRow("test1", 6, "DEF")

	// Tell Mock DB what to expect - select + row data
	mock.ExpectQuery("^SELECT (.+)").WillReturnRows(rows)

	// Call handler function to check db
	res := makeRequest(a, "GET", "/cards", nil)

	// Expected Data
	data := []models.Card{
		{
			Name:     "test",
			Quantity: 5,
			Set:      "ABC",
		},
		{
			Name:     "test1",
			Quantity: 6,
			Set:      "DEF",
		},
	}

	var got []models.Card
	json.Unmarshal(res.Body.Bytes(), &got)

	if diffEquals := cmp.Diff(got, data); diffEquals != "" {
		t.Errorf("Output not matched - %s", diffEquals)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("Expectations not met: %s", err)
	}
}

func TestDeleteCardHandler(t *testing.T) {
	a, mock := newTestSetup()
	defer a.DB.Close()

	sqlmock.NewRows([]string{"name", "quantity", "set"}).
		AddRow("test", 5, "ABC")

	mock.ExpectBegin()
	mock.ExpectExec("^DELETE (.+)").WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectCommit()

	res := makeRequest(a, "DELETE", "/cards/1", nil)

	// Check status code
	if res.Code != http.StatusNoContent {
		t.Errorf("Expected Status Code %d got %d", http.StatusNoContent, res.Code)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("Expectations not met: %s", err)
	}
}

func TestUpdateCardHandler(t *testing.T) {
	a, mock := newTestSetup()
	defer a.DB.Close()

	sqlmock.NewRows([]string{"name", "quantity", "set"}).
		AddRow("test", 5, "ABC")

	mock.ExpectBegin()
	mock.ExpectExec(`^UPDATE "cards" SET (.+) "name" (.+), "set" (.+), "quantity" (.+)`).WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectCommit()

	data := []byte(`{"name":"test","quantity":6, "set": "ABC"}`)
	body := bytes.NewReader(data)

	res := makeRequest(a, "PUT", "/cards/1", body)

	if res.Code != http.StatusNoContent {
		t.Errorf("Expected Status Code %d got %d", http.StatusNoContent, res.Code)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("Expectations not met: %s", err)
	}
}

func TestUserRegistration(t *testing.T) {
	a, mock := newTestSetup()
	defer a.DB.Close()

	emptyRow := sqlmock.NewRows([]string{"id", "username", "password"})
	insertRow := sqlmock.NewRows([]string{"id", "username", "password"}).
		AddRow(1, "name", "password")

	mock.ExpectQuery("^SELECT (.+)").WillReturnRows(emptyRow)
	mock.ExpectBegin()
	mock.ExpectQuery("^INSERT (.+)").WillReturnRows(insertRow)
	existingRow := sqlmock.NewRows([]string{"id", "username", "password"}).
		AddRow(1, "name", "password")

	mock.ExpectQuery("^SELECT (.+)").WillReturnRows(existingRow)

	data := []byte(`{"username": "name", "password", "password"}`)
	body := bytes.NewReader(data)
	res := makeRequest(a, "POST", "/users", body)

	if res.Code != http.StatusOK {
		t.Errorf("Expected Status Code %d got %d", http.StatusOK, res.Code)
	}

	data = []byte(`{"username": "name", "password", "password"}`)
	body = bytes.NewReader(data)
	res = makeRequest(a, "POST", "/users", body)

	if res.Code != http.StatusConflict {
		t.Errorf("Expected Status Code %d got %d", http.StatusConflict, res.Code)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("Expectations not met: %s", err)
	}
}

func TestUserAuthentication(t *testing.T) {
	a, mock := newTestSetup()
	defer a.DB.Close()

	password, _ := bcrypt.GenerateFromPassword([]byte("password"), 10)
	row := sqlmock.NewRows([]string{"id", "username", "password"}).
		AddRow(1, "name", password)
	mock.ExpectQuery("^SELECT (.+)").WillReturnRows(row)

	data := []byte(`{"username": "name", "password": "password"}`)
	body := bytes.NewReader(data)
	res := makeRequest(a, "POST", "/users/auth", body)

	if res.Code != http.StatusOK {
		t.Errorf("Expected Status Code %d got %d", http.StatusOK, res.Code)
	}

	data = []byte(`{"username": "name", "password": "wrongpassword"}`)
	body = bytes.NewReader(data)
	res = makeRequest(a, "POST", "/users/auth", body)

	if res.Code != http.StatusUnauthorized {
		t.Errorf("Expected Status Code %d got %d", http.StatusUnauthorized, res.Code)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("Expectations not met: %s", err)
	}
}
