package models

import (
	"time"

	"github.com/jinzhu/gorm"
)

type Card struct {
	ID        uint `gorm:"primary_key"`
	CreatedAt time.Time
	UpdatedAt time.Time
	Name      string `json:"name"`
	Set       string `json:"set"`
	Quantity  int    `json:"quantity"`
	User      User
}

type Set struct {
	ID        uint `gorm:"primary_key"`
	CreatedAt time.Time
	UpdatedAt time.Time
	Code      string `json:"code"`
	Name      string `json:"name"`
}

type User struct {
	gorm.Model
	Username string `json:"username"`
	Password string `json:"password"`
}

type Session struct {
	gorm.Model
	User  User
	Token string
}

func Migrate(db *gorm.DB) {
	db.AutoMigrate(&Card{}, &User{}, &Session{})
}
