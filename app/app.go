package app

import (
	"log"
	"net/http"

	"gitlab.com/rmoe/tradebinder/app/handlers"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
)

type App struct {
	Router *mux.Router
	DB     *gorm.DB
}

func (a *App) Initialize(db *gorm.DB) {
	a.DB = db
	a.Router = mux.NewRouter()

	a.Router.HandleFunc("/users", a.handleRequest(handlers.AddUser)).Methods("POST")
	a.Router.HandleFunc("/users/auth", a.handleRequest(handlers.AuthUser)).Methods("POST")
	a.Router.HandleFunc("/cards", a.handleRequest(handlers.AddCard)).Methods("POST")
	a.Router.HandleFunc("/cards", a.handleRequest(handlers.ListCards)).Methods("GET")
	a.Router.HandleFunc("/cards/{id}", a.handleRequest(handlers.DeleteCard)).Methods("DELETE")
	a.Router.HandleFunc("/cards/{id}", a.handleRequest(handlers.UpdateCard)).Methods("PUT")
}

func (a *App) Run(host string) {
	log.Fatal(http.ListenAndServe(host, a.Router))
}

type RequestHandlerFunction func(db *gorm.DB, w http.ResponseWriter, r *http.Request)

func (a *App) handleRequest(f RequestHandlerFunction) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		f(a.DB, w, r)
	}
}
