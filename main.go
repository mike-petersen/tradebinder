package main

import (
	"fmt"
	"log"

	env "github.com/Netflix/go-env"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"gitlab.com/rmoe/tradebinder/app"
	"gitlab.com/rmoe/tradebinder/app/models"
)

type Environment struct {
	Host        string `env:"HOST"`
	Port        int    `env:"PORT"`
	Username    string `env:"USERNAME"`
	Password    string `env:"PASSWORD"`
	Database    string `env:"DATABASE"`
	SSLMode     string `env:"SSLMODE"`
	SSLRootCert string `env:"SSLROOTCERT"`
}

func main() {
	var environment Environment
	_, err := env.UnmarshalFromEnviron(&environment)
	if err != nil {
		log.Fatal(err)
	}

	dbStr := fmt.Sprintf(
		"host=%s port=%d user=%s password=%s dbname=%s sslmode=%s sslrootcert=%s",
		environment.Host, environment.Port, environment.Username, environment.Password,
		environment.Database, environment.SSLMode, environment.SSLRootCert,
	)

	db, err := gorm.Open("postgres", dbStr)
	if err != nil {
		log.Fatal(err)
	}

	models.Migrate(db)
	a := app.App{}
	a.Initialize(db)
	a.Run(":8080")
}
