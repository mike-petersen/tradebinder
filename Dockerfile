FROM golang:1.12 as builder

ENV GO111MODULE=on
WORKDIR /go/src/tradebinder

COPY go.sum .
COPY go.mod .

RUN go mod download

COPY . .

RUN go build

FROM golang:1.12

COPY --from=builder /go/src/tradebinder/tradebinder .
EXPOSE 8080

CMD ["/go/tradebinder"]